$(document).ready(function () {

	$('.multiple-items').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,

				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		]
	});

	// SLIDER
	$(".center-slider").slick({
		dots: false,
		infinite: true,
		arrows: true,
		centerMode: true,
		centerPadding: "70px",
		speed: 300,
		slidesToShow: 1,
		adaptiveHeight: true,
		slidesToScroll: 1,
	});


	$('.center-slide.active').nextUntil().addClass('next-slide');
	$('.center-slide.active').prevUntil().addClass('prev-slide');

	$('.center-slider-sec .slick-next, .center-slider-sec .slick-prev').click(function () {
		$('.slick-center.slick-current').nextUntil().addClass('slick-right');
		$('.slick-center.slick-current').prevUntil().addClass('slick-left');
		$('.slick-center.slick-current').removeClass('slick-left');
		$('.slick-center.slick-current').removeClass('slick-right');
	});
	$('.custom-slider').append('<div class="arrows"><div class="slider-arrow arrow-left"></div><div class="arrow-right slider-arrow"></div></div>');


	$('.arrow-right.slider-arrow').click(function () {
		$(this).parent().siblings('.center-slide.active').next().addClass('active').siblings().removeClass('active');
		$(this).parent().siblings('.center-slide.active').removeClass('next-slide');
		$(this).parent().siblings('.center-slide.active').prev().addClass('prev-slide');
		$(this).parent().siblings('.center-slide.active').next().css({ 'z-index': '9' });
		$(this).parent().siblings('.center-slide.active').prev().css({ 'z-index': '9' });
		$(this).siblings().fadeIn();
		$(this).siblings().css({ 'z-index': '9999' });
		if ($(this).parent().siblings('.center-slide:last').hasClass('active')) {
			$(this).fadeOut();
			$(this).css({ 'z-index': '-1' });

		}
	})
	$('.arrow-left.slider-arrow').click(function () {
		$(this).parent().siblings('.center-slide.active').prev().addClass('active').siblings().removeClass('active');
		$(this).parent().siblings('.center-slide.active').removeClass('prev-slide');
		$(this).parent().siblings('.center-slide.active').next().addClass('next-slide');
		$(this).parent().siblings('.center-slide.active').next().css({ 'z-index': '9' });
		$(this).parent().siblings('.center-slide.active').prev().css({ 'z-index': '9' });
		$(this).siblings().fadeIn();
		$(this).siblings().css({ 'z-index': '9999' });
		if ($(this).parent().siblings('.center-slide:first').hasClass('active')) {
			$(this).fadeOut();
			$(this).css({ 'z-index': '-1' });
		}
	})



	$(".toggle-btn").click(function () {
		$(".toggle-btn").toggleClass("toggleActive");
		$("body").toggleClass('menuActive');
	});


	var headerHeight = $('.site-header').outerHeight(true);


	$('.site-navigation').css('top', headerHeight);
	$(window).on('ready resize load', function () {
		var headerHeight = $('.site-header').outerHeight(true);
		$('.site-navigation').css('top', headerHeight);
	});
	$('.site-navigation li a').click(function (e) {
		e.preventDefault();
		var getOffset = $(this).attr('href');
		$('html').animate({ scrollTop: $(getOffset).offset().top }, 500)
	});


	scrollTop = $(window).scrollTop();
	$('.site-header').after('<div class="space"></div>');
	$(window).bind('scroll', function () {
		if ($(this).scrollTop() > headerHeight) {
			$('.site-header').addClass('fixed-header')
			$('.space').css('height', headerHeight);
		}
		else {

			$('.site-header').removeClass('fixed-header')
			$('.space').css("height", 'auto ');
		}

	})


	$('#submitform').click(function (event) {
		event.preventDefault();

		if ($('#username').val() == '') {
			$('#username').addClass('required');
			return false;
		} else {
			$('#username').removeClass('required');
		}

		if ($('#emailadress').val() == '') {
			$('#emailadress').addClass('required');
			return false;
		}
		else {
			$('#emailadress').removeClass('required');
		}


		$.ajax({
			url: "./email.php",
			method: 'post',
			data: $('#contactform').serialize(),
			success: function (result) {
				$('#contactform').hide();
				$('#showsuccess').css({ 'display': 'block' });
			}
		});



	});

});
